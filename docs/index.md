# TIC - Installation des Logiciels de Base

## Introduction

Ce site vous explique comment installer les logiciels qui seront utilisés dans le cadre de certains cours en filières Informatique et Télécommunications.

Nous vous invitons à installer ces logiciels en suivant les indications ci-après. Cela vous permettra ainsi de gagner du temps et de pouvoir utiliser plus rapidement ces logiciels lors des travaux pratiques (laboratoires).

Si vous ne vous sentez pas apte à effectuer ces installations ou si vous rencontrez des problèmes, une assistance sera proposée lors de la première séance de laboratoire.

Les instructions d'installation sont données pour le système d'exploitation Windows et Mac OSX. Pour Linux, les étudiants se chargeront eux-mêmes de trouver et d'installer les logiciels correspondants (ils existent). La phase de configuration d'Intellij est la même pour tous les systèmes.

Les installations doivent être effectuées dans l'ordre proposé ci-dessous.

## Installation de Java

Pour les cours de programmation, nous utiliserons la version 8 de Java. En effet, les versions 9 et 10 sont des versions à «court terme» et la version 11 (qui sortira le 25 septembre 2018) apporte des changements que nous n'avons pas encore évalués.

* Télécharger la dernière «release» de la version 8 du «[Java SE Development Kit](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)» qui correspond à votre système d'exploitation. Notez que vous devez sélectionner "Accept License Agreement" pour effectuer le téléchargement

* Lancer ensuite le programme d'installation que vous venez de télécharger
* Suivre les indications de l'assistant d'installation en conservant les valeurs proposées par défaut
* Notez l'endroit où sera installé votre jdk. Dans l'exemple ci-dessus, c'est `C:\Program Files\Java\jdk.1.8.0_NNN\` (NNN = version du jdk)

## Installation et Configuration de IntelliJ IDEA

* Pour les cours de première année, nous utiliserons le logiciel IntelliJ IDEA de [Jetbrains](https://www.jetbrains.com/). Vous pouvez n'installer ce logiciel directement si vous voulez, mais pour simplifier les mises à jour, nous vous proposons d'installer le «toolbox». [^1]
* Se rendre sur le site de Jet Brain pour [télécharger la version du toolbox](https://www.jetbrains.com/toolbox/app/) qui correspond à votre système d'exploitation

![toolbox](images/toolbox.png)

* Installez et démarrez le logiciel en laissant les paramètres par défaut. Quand le logiciel vous propose de vous inscrire, cliquez sur «skip». Vous pourrez configurer votre compte «edu.hefr.ch» plus tard
* Le Toolbox est accessible dans la barre des tâches de votre système. Cliquez sur l'icône du «toolbox» et installez «IntelliJ IDEA Community»

![toolbox](images/toolbox1.png)

* Dès qu’«IntelliJ IDEA Community» sera installé, il apparaîtra tout en haut de la liste des outils.
* Cliquez sur «IntelliJ IDEA Community» pour démarrer l'application

![intelliJ](images/ij.png)

* Créez un nouveau projet (Create New Project)

![intelliJ](images/ij1.png)

* Le logiciel vous demande le «Project SDK». Cliquez sur «New...» et sélectionnez le répertoire dans lequel vous avez installé le JDK Java.

![intelliJ](images/ij2.png)

* Après avoir choisi le JDK, cliquez sur «Next». Le système vous propose ensuite d'utiliser un «template». Ne sélectionnez rien et cliquez sur «Next»
* Nommez le projet «PR1» et choisissez le répertoire qui sera utilisé pour le projet (le répertoire proposé par défaut fera probablement bien l'affaire)

![pr1](images/pr1.png)

* Après quelques secondes, vous serez prêt à écrire votre premier programme. Dans le panneau de gauche, cliquez sur «PR1» pour ouvrir le projet, puis cliquez avec le bouton de droite de la souris et sélectionnez «New --> Package». Nommez votre package «intro».
* Cliquez sur «intro», puis, avec le bouton de droite de la souris, sélectionnez «New --> Java Class». Nommez votre classe «hello»
* Entrez le code suivant :

``` java
package intro;

public class hello {

    public static void main(String[] args) {
        System.out.println("Hello Java!");
    }
}
```

Cliquez sur le triangle vert à gauche de «public static ...» et vous devrez voir le résultat suivant:

![hello](images/hello.png)

Voilà, vous êtes maintenant prêt pour faire les premiers travaux pratiques. Lisez encore le [tutoriel](https://www.jetbrains.com/help/idea/creating-and-running-your-first-java-application.html) sur le site de Jetbrains pour avoir plus d'information sur la création de votre premier programme Java.

!!! note
    Si vous avez des questions, des remarques, des suggestions, ou si vous voyez des des erreurs sur cette page, n'hésitez pas à m'envoyer un e-mail à l'adresse <jacques.supcik@hefr.ch>.

[^1]: Quand vous aurez votre compte «edu.hefr.ch», vous pourrez vous inscrire et profiter **gratuitement** de tous les logiciels de Jetbrains.